rm doc/setting.html
rm doc/final.html

mkdir -p ./doc/default_modPack

for mod in (ls ./mods );
  if test -d ./mods/$mod;
    if test -d ./doc/default_modPack/$mod;
      rm -r ./doc/default_modPack/$mod
    ;end
    if test -d ./mods/$mod/documentation;
      mkdir -p ./doc/default_modPack/$mod
      cp ./mods/$mod/documentation/* ./doc/default_modPack/$mod -r
    ;end
  ;end
;end

python3 settingDoc.py > doc/setting.html
cd doc
find | grep .md > fileToCompile.txt
find | grep .html >> fileToCompile.txt
find | grep .txt >> fileToCompile.txt

python3 docMaker.py > final.html
