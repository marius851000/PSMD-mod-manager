Gates Free-Cam Mod/Hack
By EddyK28


Installation:

If you're using Luma3DS, make sure to enable game patching (see https://github.com/AuroraWright/Luma3DS/wiki/Optional-features), and place the provided "RomFS" folder in the "luma/titles/00040000000BA800" folder on your 3DS SD card (you may have to create one of more these folders).

If you want to patch a ROM file, you're mostly on your own.  However, I do suggest to try using the .Net 3DS Toolkit (https://github.com/evandixon/DotNet3dsToolkit/releases/latest)



Use:

Hold the "L" or "X" button while opening the "Others" menu to activate Free-Cam.  The lower screen shows the controls for the current camera mode, which can be changed through the menu.  You can also change the species of the hero and partner Pokemon with the "PKID Menu", although this should not be used for regular gameplay (it messes up your stats).

WARNING: Just to be safe, don't save you game after having used this.  This has been tested to not immediately destroy your game, but I can in no way guarantee that it won't eventually muck up your save, kill your console, burn down your house or destroy the entire universe.

Also, please note, the camera and character always move in world coordinates, not relative to the current camera position.  This could be changed, but I don't really feel like it.  If you like, feel free to make any modifications to the code, or use it as a loader for your own menu based mods (see below).



Misc:

The main code of this mod/hack is stored in "script/mods/main.lua".  This code can be modified to alter Free-Cam's behavior, or entirely replaced to make your own menu based mod.  You could even write some code to selectably load other files from a menu to make a whole mod loader!

"menu_dungeon.lua" is a modified version an original game script, and should probably not be altered unless you know what you're doing (or like breaking things).
