this modPack is created by marius851000, using some code from EddyK28. All of the menu.lua file is under MIT licence.

This modPack add a menu in which you can lauch any compatible modPack script which can run at any moment, for example the free cam mod.
Please note that this modPack doesnt offer the any way to open it, so you should use another modPack to open it ( take a look at [menu_loader_other](default_modPack/menu_loader_other/Readme.txt)).

To add a script in this mod menu, see the [entrypoint db page](modPack/db/entrypoint.md)
