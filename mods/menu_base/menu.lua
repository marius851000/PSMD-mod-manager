local function splitByChunke(text, chunkSize)
  local s = {}
  for i=1, #text, chunkSize do
        s[#s+1] = text:sub(i,i+chunkSize - 1)
  end
  return s
end

function runExterne(program)
  local fun, err = loadfile(program)

  if not fun then
    WINDOW:SysMsg(err)
  end

  local stat, err = pcall(fun)
  if not stat then
	err = splitByChunke(err,50)
	  for i,v in ipairs(err) do
        WINDOW:SysMsg(v)
      end
  end
  --WINDOW:CloseMessage()
end

local function menu(defaultCursor)
  local avalaibleOption = getAllentrypointList()
  WINDOW:CloseMessage()
  WINDOW:SelectStart()
  WINDOW:SelectChain("Exit",0)
  local elem = 1
  local avalaibleOption = getAllentrypointList()
  --while avalaibleOption[elem] do
  --  WINDOW:SysMsg(avalaibleOption[elem][2])
  --  WINDOW:CloseMessage()
  --  elem = elem + 1
  --  -- WINDOW:SelectChain(avalaibleOption[elem][2],elem)
  --end
  for k,v in pairs(avalaibleOption) do
    WINDOW:SelectChain(v[2],k)
    --WINDOW:SysMsg(avalaibleOption[v][1])
  end
  WINDOW:DefaultCursor(0)
  local id = WINDOW:SelectEnd(MENU_SELECT_MODE.DISABLE_CANCEL)
  if id == 0 then
    return 0
  else
    runExterne(avalaibleOption[id][1])
    return id
  end
end

--MAIN--
  --clear upper screen


  CommonSys:EndLowerMenuNavi(true)
  local retour = 1
  while retour ~= 0 do
    retour = menu(retour)
  end
  --reopen upper screen info
  CommonSys:BeginLowerMenuNavi_GroundItem(GROUND:GetWorldContinentName(), GROUND:GetWorldPlaceName(), true, true)
