--MIT License
--Copyright (c) 2018 EddyK28

local dist = 40
local height = 10
local orbChar = "HERO"
local spdist = 1
local camFOV = 30


local function menuPKID()
  local setlvl = 0
  repeat
    WINDOW:CloseMessage()
    WINDOW:SysMsg("Select_Pokemon:")
    WINDOW:SelectStart()				--create selection menu
    WINDOW:SelectChain("Done", 0)		--add entry (text, id)
    WINDOW:SelectChain("Hero", 1)
    WINDOW:SelectChain("Partner", 2)
	WINDOW:SelectChain("Force_Level", 3)
    WINDOW:DefaultCursor(0)				--set default entry
    local id = WINDOW:SelectEnd(MENU_SELECT_MODE.DISABLE_CANCEL)	--wait for and get window selection, store as "id"

    if id == 1 then
      local oldpkid = SymAct("HERO"):GetIndex()						--get "hero" and "get index"? (gives current PKID)
	  local newpkid
      local menu      = MENU:CreateNumericMenuWindow(ScreenType.A)	--create a numeric entry window
      menu:SetLayoutRect(Rectangle(96, 80, 132, 84))				--set window position/size
      menu:SetPlace(3)												--
      menu:SetDigit(3)												--
      menu:SetStartNum(oldpkid)										--set initial value
      menu:SetType(NUM_MENU_TYPE.TYPE_DIGIT_ON)						--set some flags for the menu?
      menu:SetTextOffset(24, 40)									--position menu text?
      function menu:decideAction()									--set menu accept <callback?> function
        newpkid = self:GetSettingData()									--get & store selection value
        GROUND:SetHero(newpkid, setlvl)									--set our character
        CHARA:ReloadHeroPartner()										--Refresh characters somehow?
        self:Close()													--close menu
      end
      function menu:cancelAction()									--set menu cancel <callback?> function
        self:Close()													--close menu
      end
      menu:Open()													--open menu
      menu:SetCaption("Hero Pokemon ID")							--set menu caption
      MENU:SetFocus(menu)
      MENU:WaitClose(menu)

    elseif id == 2 then
	  local oldpkid = SymAct("PARTNER"):GetIndex()					--get "partner" and "get index"? (gives current PKID)
	  local newpkid
      local menu      = MENU:CreateNumericMenuWindow(ScreenType.A)	--create a numeric entry window
      menu:SetLayoutRect(Rectangle(96, 80, 132, 84))				--set window position/size
      menu:SetPlace(3)												--
      menu:SetDigit(3)												--
      menu:SetStartNum(oldpkid)										--set initial value
      menu:SetType(NUM_MENU_TYPE.TYPE_DIGIT_ON)						--set some flags for the menu?
      menu:SetTextOffset(24, 40)									--position menu text?
      function menu:decideAction()									--set menu accept <callback?> function
        newpkid = self:GetSettingData()									--get & store selection value
        GROUND:SetPartner(newpkid, setlvl)								--set partner character
        CHARA:ReloadHeroPartner()										--Refresh characters somehow? <----
        self:Close()													--close menu
      end
      function menu:cancelAction()									--set menu cancel <callback?> function
        self:Close()													--close menu
      end
      menu:Open()													--open menu
      menu:SetCaption("Partner Pokemon ID")							--set menu caption
      MENU:SetFocus(menu)
      MENU:WaitClose(menu)

	elseif id == 3 then
      local menu      = MENU:CreateNumericMenuWindow(ScreenType.A)	--create a numeric entry window
      menu:SetLayoutRect(Rectangle(96, 80, 132, 84))				--set window position/size
      menu:SetPlace(3)												--
      menu:SetDigit(3)												--
      menu:SetStartNum(setlvl)										--set initial value
      menu:SetType(NUM_MENU_TYPE.TYPE_DIGIT_ON)
      menu:SetTextOffset(24, 40)
      function menu:decideAction()									--set menu <callback?> functions
        setlvl = self:GetSettingData()
        self:Close()
      end
      function menu:cancelAction()
        self:Close()
      end
      menu:Open()
      menu:SetCaption("Forced Level")
      MENU:SetFocus(menu)
      MENU:WaitClose(menu)
    end
  until id == 0
end

local function menuOption(tid)
  repeat
    WINDOW:CloseMessage()               --close any open messages
    WINDOW:SelectStart()				--create selection menu
    WINDOW:SelectChain("Done", 0)		--add entry (text, id)
    --WINDOW:SelectChain("PKID_Menu", 1)
    if tid ~= 1 then
      WINDOW:SelectChain("Switch_to_Char_Orbit", 2)
    end
    if tid ~= 2 then
      WINDOW:SelectChain("Switch_to_Spot_Orbit", 3)
    end
    if tid ~= 3 then
      WINDOW:SelectChain("Switch_to_Cam_Target", 4)
    end
    WINDOW:SelectChain("Get_Height", 5)
    WINDOW:SelectChain("Exit", 8)
    WINDOW:DefaultCursor(0)				--set default entry

    local id = WINDOW:SelectEnd(MENU_SELECT_MODE.DISABLE_CANCEL)	--wait for and get window selection, store as "id"

    if id == 1 then
      menuPKID()

    elseif id == 2 then --TODO: use range and return id -1
      return 1

	elseif id == 3 then
      return 2

    elseif id == 4 then
      return 3

    elseif id == 5 then
      WINDOW:SysMsg("Height: "..height)
      WINDOW:CloseMessage()
    elseif id == 8 then
      return 0
    end
  until id == 0

  return tid
end


local function orbitChar()
  --Create info box on lower screen
  local infoBox = MENU:CreateBoardMenuWindow(ScreenType.B)
  infoBox:SetLayoutRect(Rectangle(32, 32, 256, 192))
  infoBox:SetText("D-Pad U/D: Move up/down\nD-Pad L/R: Move in/out\n\nStick:\n  A: Hold to Orbit\n  X: Hold to Move Character\n  B: Hold to Rotate Character / Zoom\n\nL: Slow Movement Modifier\nY: Switch Hero/Partner\nR: Move character to cam height\nStart: Menu")
  infoBox:SetTextOffset(1, 1)
  infoBox:Open()

  --move camera to target
  CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

  while true do     --wait for button presses
    TASK:Sleep(TimeSec(0.01))
    if PAD:Data("START") then   --open menu
      infoBox:Close()
      return

    elseif PAD:Data("Y") then   --toggle cam target
      if orbChar == "HERO" then
        orbChar = "PARTNER"
      else
        orbChar = "HERO"
      end
      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))
      while PAD:Data("Y") do
        TASK:Sleep(TimeSec(0.01))
      end

    elseif PAD:Data("A&L") then --orbit target slow
      CAMERA:SetEyeR(Degree(-PAD:StickY()), Degree(PAD:StickX()))
      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

    elseif PAD:Data("A") then   --orbit target
      CAMERA:SetEyeR(Degree(PAD:StickY()*-3), Degree(PAD:StickX()*3))
      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

    elseif PAD:Data("B&L") then  --char rotate slow?
      CH(orbChar):DirTo(RotateOffs(PAD:StickX()*3), Speed(1800))
      if PAD:Data("(UP|DOWN)") then
        if PAD:Edge("UP") then
          camFOV = camFOV - 1

        elseif PAD:Edge("DOWN") then
          camFOV = camFOV + 1
        end

        CAMERA:SetFovy(Degree(camFOV))

        while PAD:Data("(UP|DOWN)") do
          TASK:Sleep(TimeSec(0.01))
        end
      end

    elseif PAD:Data("B") then   --char rotate?
      CH(orbChar):DirTo(RotateOffs(PAD:StickX()*6), Speed(1800))
      if PAD:Data("(UP|DOWN)") then
        if PAD:Edge("UP") then
          camFOV = camFOV - 10

        elseif PAD:Edge("DOWN") then
          camFOV = camFOV + 10
        end

        CAMERA:SetFovy(Degree(camFOV))

        while PAD:Data("(UP|DOWN)") do
          TASK:Sleep(TimeSec(0.01))
        end
      end

    elseif PAD:Data("X&L") then --move character slow
      CH(orbChar):MoveTo(PosOffs(PAD:StickX()/6, -PAD:StickY()/6), Speed(4))
      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

    elseif PAD:Data("X") then   --move character
      CH(orbChar):MoveTo(PosOffs(PAD:StickX(), -PAD:StickY()), Speed(4))
      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

    elseif PAD:Data("R") then   --move target to cam height
      CH(orbChar):MoveHeightTo(Height(height/10), Speed(5))
      CH(orbChar):WaitMoveHeight()

    elseif PAD:Data("(RIGHT|LEFT|UP|DOWN)") then
      if PAD:Edge("RIGHT") then
        if PAD:Data("L") then
          dist = dist + 1
        else
          dist = dist + 5
        end

      elseif PAD:Edge("LEFT") then
        if PAD:Data("L") then
          dist = dist - 1
        else
          dist = dist - 5
        end

      elseif PAD:Edge("UP") then
        if PAD:Data("L") then
          height = height + 0.1
        else
          height = height + 1
        end

      elseif PAD:Edge("DOWN") then
        if PAD:Data("L") then
          height = height - 0.1
        else
          height = height - 1
        end
      end

      CAMERA:MoveFollowZoom(CH(orbChar), Height(height/10), Distance(dist/10), Speed(10))

      while PAD:Data("(RIGHT|LEFT|UP|DOWN)") do
        TASK:Sleep(TimeSec(0.01))
      end

    else

    end
  end

  infoBox:Close()
end

local function orbitSpot()

  --Create info box on lower screen
  local infoBox = MENU:CreateBoardMenuWindow(ScreenType.B)
  infoBox:SetLayoutRect(Rectangle(32, 32, 256, 192))
  infoBox:SetText("D-Pad U/D: Move up/down\nD-Pad L/R: Move in/out\n\nStick:\n  A: Hold to Look\n  X: Hold to Move Camera\n  B: Hold to Zoom\n\nL: Slow Movement Modifier\nStart: Menu")
  infoBox:SetTextOffset(1, 1)
  infoBox:Open()


  while true do     --wait for button presses
    TASK:Sleep(TimeSec(0.01))
    if PAD:Data("START") then   --open menu
      infoBox:Close()
      return

    elseif PAD:Data("B") then

      if PAD:Data("(UP|DOWN)") then
        if PAD:Edge("UP") then
          if PAD:Data("L") then
            camFOV = camFOV - 1
          else
            camFOV = camFOV - 10
          end

        elseif PAD:Edge("DOWN") then
          if PAD:Data("L") then
            camFOV = camFOV + 1
          else
            camFOV = camFOV + 10
          end
        end

        CAMERA:SetFovy(Degree(camFOV))

        while PAD:Data("(UP|DOWN)") do
          TASK:Sleep(TimeSec(0.01))
        end
      end

    elseif PAD:Data("A&L") then --orbit target slow
      CAMERA:SetEyeR(Degree(PAD:StickY()), Degree(-PAD:StickX()))
      CAMERA:MoveFollowZoomR(Vector(0, 0, 0), Distance(spdist/10), Speed(10))

    elseif PAD:Data("A") then   --orbit target
      CAMERA:SetEyeR(Degree(PAD:StickY()*3), Degree(PAD:StickX()*-3))
      CAMERA:MoveFollowZoomR(Vector(0, 0, 0), Distance(spdist/10), Speed(10))

    elseif PAD:Data("X&L") then   --camera move slow
      CAMERA:MoveFollowZoomR(Vector(PAD:StickX()/6, 0, -PAD:StickY()/6), Distance(spdist/10), Speed(10))

    elseif PAD:Data("X") then   --camera move
      CAMERA:MoveFollowZoomR(Vector(PAD:StickX()/3, 0, -PAD:StickY()/3), Distance(spdist/10), Speed(10))

    elseif PAD:Data("(RIGHT|LEFT|UP|DOWN)") then
      if PAD:Edge("RIGHT") then
        if PAD:Data("L") then
          spdist = spdist + 1
        else
          spdist = spdist + 5
        end
        CAMERA:MoveFollowZoomR(Vector(0, 0, 0), Distance(spdist/10), Speed(10))

      elseif PAD:Edge("LEFT") then
        if PAD:Data("L") then
          spdist = spdist - 1
        else
          spdist = spdist - 5
        end
        CAMERA:MoveFollowZoomR(Vector(0, 0, 0), Distance(spdist/10), Speed(10))

      elseif PAD:Edge("UP") then
        if PAD:Data("L") then
          CAMERA:MoveFollowZoomR(Vector(0, 0.1, 0), Distance(spdist/10), Speed(10))
        else
          CAMERA:MoveFollowZoomR(Vector(0, 1, 0), Distance(spdist/10), Speed(10))
        end

      elseif PAD:Edge("DOWN") then
        if PAD:Data("L") then
          CAMERA:MoveFollowZoomR(Vector(0, -0.1, 0), Distance(spdist/10), Speed(10))
        else
          CAMERA:MoveFollowZoomR(Vector(0, -1, 0), Distance(spdist/10), Speed(10))
        end
      end

      while PAD:Data("(RIGHT|LEFT|UP|DOWN)") do
        TASK:Sleep(TimeSec(0.01))
      end

    else

    end
  end
end


local function tgtEye()
  local campos = CAMERA:GetEye()
  local camtgt = CAMERA:GetTgt()
  local camofs = Vector(0, 0, 0)
  local mult = 1

  local posStr = string.format("%.1f, %.1f, %.1f", campos.x, campos.y, campos.z)
  local tgtStr = string.format("%.1f, %.1f, %.1f", camtgt.x, camtgt.y, camtgt.z)

  --Create info box on lower screen
  local infoBox = MENU:CreateBoardMenuWindow(ScreenType.B)
  infoBox:SetLayoutRect(Rectangle(32, 32, 256, 192))
  infoBox:SetText("D-Pad:\n  Default: Move Target\n  A: Move Camera\n  L: Slow Movement Modifier\n  R: Vertical Movement Modifier\n\nX: Mover Char to Target\nY: Reset Camera\nStart: Menu\n\nCamera: " .. posStr .. "\nTarget: " .. tgtStr)
  infoBox:SetTextOffset(1, 1)
  infoBox:Open()

  while true do     --wait for button presses
    TASK:Sleep(TimeSec(0.01))
    if PAD:Data("START") then   --open menu
      infoBox:Close()
      return
    elseif PAD:Data("X") then
      if PAD:Data("R") then
        CH("PARTNER"):MoveTo(Vector2(camtgt.x, camtgt.z),Speed(350))
      else
        CH("HERO"):MoveTo(Vector2(camtgt.x, camtgt.z),Speed(350))
      end
    elseif PAD:Data("Y") then
      CAMERA:MoveEye(Vector(0, 5, 5), Speed(5))
      CAMERA:MoveTgt(Vector(0, 0, 0), Speed(5))
      CAMERA:WaitMove()
      campos = CAMERA:GetEye()
      camtgt = CAMERA:GetTgt()
      posStr = string.format("%.1f, %.1f, %.1f", campos.x, campos.y, campos.z)
      tgtStr = string.format("%.1f, %.1f, %.1f", camtgt.x, camtgt.y, camtgt.z)
      infoBox:SetText("D-Pad:\n  Default: Move Target\n  A: Move Camera\n  L: Slow Movement Modifier\n  R: Vertical Movement Modifier\n\nX: Mover Char to Target\nY: Reset Camera\nStart: Menu\n\nCamera: " .. posStr .. "\nTarget: " .. tgtStr)
    elseif PAD:Data("(RIGHT|LEFT|UP|DOWN)") then
      if PAD:Data("L") then
        mult = 0.1
      else
        mult = 1
      end

      if PAD:Edge("RIGHT") then
        camofs = Vector(mult, 0, 0)
      elseif PAD:Edge("LEFT") then
        camofs = Vector(-mult, 0, 0)
      elseif PAD:Edge("UP") then
        camofs = Vector(0, 0, -mult)
      elseif PAD:Edge("DOWN") then
        camofs = Vector(0, 0, mult)
      end

      if PAD:Data("R") then
        camofs = Vector(0, -camofs.z, 0)
      end

      if PAD:Data("A") then
        campos = campos + camofs
        CAMERA:MoveEye(campos, Speed(5))
      else
        camtgt = camtgt + camofs
        CAMERA:MoveTgt(camtgt, Speed(5))
      end

      posStr = string.format("%.1f, %.1f, %.1f", campos.x, campos.y, campos.z)
      tgtStr = string.format("%.1f, %.1f, %.1f", camtgt.x, camtgt.y, camtgt.z)
      infoBox:SetText("D-Pad:\n  Default: Move Target\n  A: Move Camera\n  L: Slow Movement Modifier\n  R: Vertical Movement Modifier\n\nX: Mover Char to Target\nY: Reset Camera\nStart: Menu\n\nCamera: " .. posStr .. "\nTarget: " .. tgtStr)

      while PAD:Data("(RIGHT|LEFT|UP|DOWN)") do
        TASK:Sleep(TimeSec(0.01))
      end

    end
  end
end


--MAIN--

  --clear upper screen
  CommonSys:EndLowerMenuNavi(true)

  --only allow spot orbit use in dungeons
  if SYSTEM:IsDungeon() then
    orbitSpot()
    return
  end
  
  local id = 2

  repeat
    if id == 1 then
      orbitChar()
    elseif id == 2 then
      orbitSpot()
    elseif id == 3 then
      tgtEye()
    end
    id = menuOption(id)
  until id == 0

  --reopen upper screen info
  --CommonSys:BeginLowerMenuNavi_GroundItem(GROUND:GetWorldContinentName(), GROUND:GetWorldPlaceName(), true, true)
  WINDOW:CloseMessage()
