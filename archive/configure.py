#!/bin/python3
#to be able to open it as a program in GNU / linux

from lxml import etree

def echoable(text,to=None):
    rendu = []
    for line in text.split("\n"):
        nc = ""
        for char in line:
            if char == "\"":
                nc += "\\\""
            else:
                nc += char
        if to != None:
            rendu.append("echo \""+nc+"\" >> "+to)
        else:
            rendu.append("echo \""+nc+"\"")
    return rendu



class modPack:
    """represent a mod pack ( a list of single modification to apply, usually in a subfolder, like camera/ or entryPoint/, with all the single mod data usually in mod.xml )."""
    def __init__(self,master,location):
        """initialize a modpack. master is the master class ( with an access to all variable in the program ). The location is the location of the folder in which it is contained ( string ). reltaive with master.modDir"""
        self.master = master
        self.mods = {} # all the modification to apply to a file specific file ( starting with romfs/ )
        self.load(location)

    def load(self,location):
        """load the file mod.xml in the folder location ( relative to master.modDir )"""
        self.location = location # the folder location ( relative to master.modDir )
        self.fullLocation = self.master.modDir+"/"+self.location+"/"+self.master.modSettingFile # the location of the configuration file
        temp = open(self.fullLocation)
        arbre = etree.fromstring(temp.read()) # getting the xml tree ( called arbre )
        temp.close()

        # getting the name ( to display )
        self.nameFound = arbre.find("name")
        if self.nameFound is not None:
            self.name = self.nameFound.text
        else:
            self.name = self.location.split("/")[-1]
            self.master.alert("WARNING", "mod without name : "+self.name+". The default name will be used ( the folder name ) .")

        # getting files to replace
        for file in arbre.findall("file"):
            if file.text == "":
                self.master.alert("ERROR", "a file name is empty for the mod "+self.name+". File ignored.")
            else:
                self.addFileMod(file.text,fileReplacement(self,file.text))
                if file.attrib.get("writeAllEntry")=="True":# check if we should write the entryPoint in this file.
                    self.addFileMod(file.text,fileAddEntry(self,file.text))

        # getting entryPoints
        self.entryPoints = []
        for entry in arbre.findall("entryPoint"):
            self.entryPoints.append({"name":entry.attrib.get("name"),"file":entry.attrib.get("file")})
            # TODO: test if file and name are present

    def addFileMod(self,file,mod):
        """link that the mod will modify the file"""
        # TODO: add the config file of mod to the requirement, regenerating all the file impacted by mod if the config change
        if not file in self.mods:
            self.mods[file] = [mod]
        else:
            self.mods[file].append(mod)

    def debug(self):
        """print some debug data about the mod"""
        print("  mod:")
        print("    location : "+self.location)
        print("    name : "+self.name)
        #print("    entryPoints :")
        # TODO: entry point
        #for entry in self.entrys:
        #    print("      entry :")
        #    print("        name : "+entry["name"])
        #    print("        file : "+entry["file"])
        print("    smods :")
        for modf in self.mods:
            print("      smodf : "+modf)
            for mod in self.mods[modf]:
                print("        smod : "+str(mod))

class fileMod:
    def __init__(self,mod,file):
        self.mod = mod # the mother mod class
        self.file = file # the file ( starting with romfs/ )

    def getFullOriginUrl(self):
        return self.mod.master.modDir+"/"+self.mod.location+"/"+self.file

    def getFullDestUrl(self):
        return self.mod.master.renderDir+"/"+self.file

    def getTemp(self,name):
        return self.mod.master.renderDir+"/"+name

    def getFullDestFolder(self):
        temp = self.getFullDestUrl().split("/")[:-1]
        rendu = ""
        for loop in temp:
            rendu += loop + "/"
        return rendu

    def getMake(self):
        return ["echo \"empty mod\""]

class fileAddEntry(fileMod):
    def getMake(self):
        funcToAdd = "local function getAllMod()\n\treturn {"
        for amod in self.mod.master.mods:
            mod = self.mod.master.mods[amod]
            for entry in mod.entryPoints:
                funcToAdd+="{\""+entry["file"]+"\",\""+entry["name"]+"\"},"
        funcToAdd = funcToAdd[0:-1]
        funcToAdd += "}\nend\n"
        print(echoable(funcToAdd))

        return {"fileModified":self.getFullDestUrl(),
            "requirement":[self.mod.fullLocation], # TODO: add all the mod.xml to the requirement list
            # TODO: find a simpler way to add text at the beginning of the file
            "step":["touch "+self.getTemp("entryTemp")]+echoable(funcToAdd,self.getTemp("entryTemp"))+["cat "+self.getFullDestUrl()+" >> "+self.getTemp("entryTemp"),"mv "+self.getTemp("entryTemp")+" "+self.getFullDestUrl()]}

class fileReplacement(fileMod):
    def getMake(self):
        # add to the requirement the config file ( thus recompute all the file impacted by the mod if the mod.xml change )
        return {"fileModified":self.getFullDestUrl(),
            "requirement":[self.getFullOriginUrl(),self.mod.fullLocation],
            "step" : ["mkdir -p "+self.getFullDestFolder(),
                "cp "+self.getFullOriginUrl()+" "+self.getFullDestUrl()]}

class master:
    settingDir = "."
    settingFile = "setting.xml"

    modDir = settingDir
    modSettingFile = "mod.xml"

    renderDir = modDir + "/render"

    def __init__(self):
        self.loadSetting()

    def loadSetting(self):
        temp = open(self.settingDir+"/"+self.settingFile)
        arbre = etree.fromstring(temp.read())
        temp.close()

        self.mods = {} # by name
        for modName in arbre.find("mods").findall("mod"):
            self.mods[modName.text] = modPack(self,modName.text)

    def createMake(self):
        # create make as a per file way
        fileToMake = {} # by the application url ( romfs/* )
        for mod in self.mods:
            Amod = self.mods[mod]
            for file in Amod.mods:
                Afile = Amod.mods[file]
                if not file in fileToMake:
                    fileToMake[file] = []
                for fileMod in Afile:
                    fileToMake[file].append(fileMod)

        # applique les mod un par un
        makeByFile = {}
        # TODO: multiple mod compatibility
        for file in fileToMake:
            Afile = fileToMake[file]
            # TODO: short the range ( cf apply add after replace, and thing like that)
            for smod in range(len(Afile)):
                if smod == 0:
                    makeByFile[file]=Afile[smod].getMake()
                else:
                    otherMake = Afile[smod].getMake()
                    if makeByFile[file]["fileModified"] != otherMake["fileModified"]:
                        raise
                    for requirement in otherMake["requirement"]:
                        if not requirement in makeByFile[file]["requirement"]:
                            makeByFile[file]["requirement"].append(requirement)
                    for step in otherMake["step"]:
                        makeByFile[file]["step"].append(step)
        ###################################
        ### make the makefile by itself ###
        ###################################
        makeFile = "" #the MakeFile

        # make the all cible
        makeFile += "all :"
        for file in makeByFile:
            fileToMake = makeByFile[file]
            makeFile += " " + fileToMake["fileModified"]

        makeFile += "\nclean :\n\trm -r render\n\trm makefile\n"

        for file in makeByFile:
            fileToMake = makeByFile[file]
            requirementText = "" # the requirement text ( placed after the name of the modified file and : in make )
            for requirement in fileToMake["requirement"]:
                requirementText += requirement+" "
            appendToMakeFile = fileToMake["fileModified"]+" : " + requirementText + "\n" # create the first line ( modifiedFile + requirement)

            for makeLine in fileToMake["step"]:
                appendToMakeFile += "\t"+makeLine+"\n"

            makeFile += appendToMakeFile
        return makeFile

    def debug(self):
        print("master :")
        for loop in self.mods:
            self.mods[loop].debug()


if __name__ == '__main__':
    a = master()
    #a.debug()
    makeFile = a.createMake()
    #print(makeFile)
    file = open("makefile","w")
    file.write(makeFile)
    file.close()
    a.debug()
