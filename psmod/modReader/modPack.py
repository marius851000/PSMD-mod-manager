from lxml import etree
from psmod.xmlParser import parseXML, xmlToDict
from psmod.configuration.main import configuration
from psmod.logger import logger

class modPack: # smaller test
    def __init__(self,modDir,conf):
        self.logger = logger()
        self.logger.log("INFO","creation of a modPack")
        self.logger.increment()
        # constante
        self.configFileName = "mod.xml"

        assert type(modDir) == str
        assert type(conf) == configuration
        self.conf = conf
        self.modDir = modDir + "/"
        self.configFile = self.modDir + self.configFileName

        self.id = self.conf.getID()
        self.conf.modData[self.id] = {"name":"dummyName",
            "modPack":self}

        self.read()

        self.logger.decrement()
        self.logger.log("MESSAGE","modPack of the name '"+self.conf.modData[self.id]["name"]+"' created")

    def read(self):
        tree = parseXML(self.configFile,self.conf.dict())
        for element in tree.findall("*"):
            if element.tag == "name":
                assert element.text != ""
                self.conf.modData[self.id]["name"] = element.text

            elif element.tag == "addDB":
                databaseElem = element.findall("db")
                assert len(databaseElem) == 1
                assert databaseElem[0].text != ""
                database = databaseElem[0].text

                valueElem = element.findall("value")
                assert len(valueElem) == 1
                self.conf.addToDB(database,xmlToDict(valueElem[0]))

            elif element.tag == "mod":
                # TODO: better dynamic load of mod
                modType = element.attrib.get("type")
                if modType == "replaceFile":
                    from psmod.mods.replaceFile import replaceFile
                    amod = replaceFile(self,element)
                elif modType == "dbCopyHead":
                    from psmod.mods.dbCopyHead import dbCopyHead
                    amod = dbCopyHead(self,element)
                else:
                    raise
            else:
                raise
