import os

class logger:
    level = []
    history = []
    printlog = True
    incrementLevel = {"value":0} # cleaner global
    depend = {"ALL":["DEBUG","INFO","MESSAGE","WARNING","ERROR","CRITICAL"],
        "DEBUG":["INFO","MESSAGE","WARNING","ERROR","CRITICAL"],
        "INFO":["MESSAGE","WARNING","ERROR","CRITICAL"],
        "MESSAGE":["WARNING","ERROR","CRITICAL"],
        "WARNING":["ERROR","CRITICAL"],
        "ERROR":["CRITICAL"]}

    def __init__(self):
        pass

    def add_level(self,level):
        assert type(level)==str, ("level must be a string")

        if not level in self.level:
            self.level.append(level)

        # auto add other element ( self.depend )
        if level in self.depend:
            for element in self.depend[level]:
                if not element in self.level:
                    self.add_level(element)

    def log(self,level,message,sup=None):
        assert type(level) == str
        assert type(message) == str

        if sup == "d":
            self.decrement()

        if level in self.level:
            if self.printlog:
                printedLevel = ""
                for loop in range(10):
                    if len(level) > loop:
                        printedLevel += level[loop]
                    else:
                        printedLevel += " "

                space = "  "*self.incrementLevel["value"]
                if level == "CRITICAL":
                    space = ""
                print(printedLevel + ":"+space + message)
                if level == "CRITICAL":
                    raise AssertionError(message)

        if sup == "i":
            self.increment()

        self.history.append({"level":level,"message":message,"increment":self.incrementLevel})

    def increment(self):
        self.incrementLevel["value"] = self.incrementLevel["value"] + 1

    def decrement(self):
        self.incrementLevel["value"] = self.incrementLevel["value"] - 1
