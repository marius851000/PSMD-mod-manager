from psmod.smods.base import singleMod

class replaceFileSingle(singleMod): # TODO: unittest
    name = "single file replacement"
    def compute(self,data):
        self.fileSource = data["fileSource"]
        self.dependance.append(self.fileSource)

    def toMake(self):
        return "cp " + self.modDir + self.fileSource + " " + self.destDir + self.file
