from psmod.logger import logger

class singleMod: # TODO: unittest
    """a base class for single mod ( mod applied to one file )"""
    def __init__(self,parent,filee,data={}):
        """filee : the modified file"""
        self.logger = logger()
        self.logger.log("DEBUG","creation of a single mod of type '"+self.name+"'.")
        self.logger.increment()
        assert type(filee) == str
        self.dependance = [] # file which the mod depend ( from the base mod folder, whitout / before )
        self.parent = parent
        self.data = data
        self.file = filee # the modified file
        self.modDir = self.parent.parent.modDir
        self.destDir = self.parent.parent.conf.destDir
        self.parent.modifiedFile.append(self)
        self.parent.parent.conf.addSingleMod(self)
        self.conf = self.parent.parent.conf

        self.compute(data)

        self.logger.decrement()
        self.logger.log("DEBUG","creation of the single finished")

    def compute(self,data):
        pass


    def toMake(self):
        """return the make parameter, no tab or line jump at the end of the line applyed"""
        return ""

    def getDataPrint(self):
        return "in the mod " + self.modDir + ", whith a single mod of the type "+self.name+", "
