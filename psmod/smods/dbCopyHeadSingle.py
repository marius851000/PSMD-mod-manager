from psmod.smods.base import singleMod

class dbCopyHeadSingle(singleMod): # TODO: unittest
    name = "single file replacement"
    def compute(self,data):
        assert type(data) == dict
        assert "db" in data
        assert "orders" in data

        self.db = data["db"]
        self.orders = data["orders"]

    def toMake(self):
        fileTemp = self.destDir + "temp_"+str(self.conf.getID())
        retour = ""
        retour += "echo \"local function getAll"+self.db+"List()\" > "+fileTemp+"\n"

        line2 = "\treturn {"
        assert self.db in self.conf.db
        for element in self.conf.db[self.db]:
            line2 += '{'
            for key in self.orders:
                assert key in element, self.logger.log("CRITICAL",self.getDataPrint()+" there is asked, in the database "+self.db+", for the element "+str(element)+", the key "+key+", which is not present in it.")
                line2 += "\\\""+element[key]+"\\\"" # TODO: traitement pour les caractére spéciaux
                line2 += ","
            line2 = line2[0:-1] + "},"
        line2 = line2[0:-1] + "}"
        retour += "echo \""+line2+"\" >> "+fileTemp+"\n"
        retour += "echo \"end\" >> "+fileTemp+"\n"
        retour += "cat "+self.destDir+self.file+" >> "+fileTemp+"\n"
        retour += "mv "+fileTemp+" "+self.destDir+self.file

        return retour

#local function getAllMod()
#	return {{"script/mods/main.lua","this menu"},{"script/mods/camera.lua","EddyK28\'s cam mod"},{"script/mods/cameraDungeon.lua","EddyK28\'s dungeon cam"}}
#end
