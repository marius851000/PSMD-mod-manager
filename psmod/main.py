class psmod:
    """classe qui s'occuppe de gérer tous les aspect du gestionnaire de mod"""
    def __init__(self,configFile,log="NONE"):
        assert log in ["NONE","ALL","DEBUG","INFO","MESSAGE","WARNING","ERROR","CRITICAL"]
        assert type(configFile) == str

        self.configFile = configFile

        from psmod.configuration.main import configuration
        self.conf = configuration(self.configFile)

        from psmod.logger import logger
        self.logger = logger()
        self.logger.add_level(log)
        self.logger.log("INFO","logger initialized")


    def run(self): # TODO: tester la somme ( toute la génération d'un mod )
        self.logger.log("MESSAGE","creation of the make begin")
        self.logger.increment()

        self.logger.log("INFO","reading the configuration file")
        self.logger.increment()
        self.logger.decrement()
        self.logger.log("MESSAGE","configuration file is succesfully read.")

        self.logger.log("MESSAGE","loading mods...")
        self.logger.increment()
        for modP in self.conf.modsEnabled:
            from psmod.modReader.modPack import modPack
            modPacked = modPack(self.conf.modDir+modP["name"],self.conf)
        self.logger.decrement()
        self.logger.log("MESSAGE","mods loaded.")

        if self.conf.getConfig("generate_make"):
            self.logger.log("INFO","generating make...")
            self.logger.increment()

            make = self.conf.toMake()

            self.logger.decrement()
            self.logger.log("MESSAGE","make is now generated.")

            if self.conf.getConfig("make/write") == True:
                self.logger.log("INFO","writing make to makefile.","i")
                a = open("makefile","w")
                a.write(make)
                a.close()
                self.logger.log("MESSAGE","file writed to makefile","d")
            else:
                self.logger.log("WARNING","file isn't writed to disk due to setting.")
        else:
            self.logger.log("WARNING","makefile isnt generated due to setting.")
        
        self.logger.decrement()
        self.logger.log("MESSAGE","make should be generated")

# TODO: command line parser
