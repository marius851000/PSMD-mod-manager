from psmod.mods.base import mod
from psmod.smods.replaceFileSingle import replaceFileSingle

class replaceFile(mod):
    name = "file replacement"
    def compute(self):
        fileSource = self.tree.find("fileSource").text
        fileDest = self.tree.find("fileDest").text

        modFinal = replaceFileSingle(self,fileDest,{"fileSource":fileSource})
