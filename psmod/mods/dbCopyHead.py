from psmod.mods.base import mod
from psmod.smods.dbCopyHeadSingle import dbCopyHeadSingle

class dbCopyHead(mod):
    name = "copy database to head of a file"
    def compute(self):
        assert len(self.tree.findall("db")) == 1, (self.getDataPrint()+"there is to many or not enought <db>...</db>")
        assert len(self.tree.findall("fileDest")) == 1, (self.getDataPrint()+"there is to many or not enought <fileDest>...</fileDest>")
        assert len(self.tree.findall("order")) >= 1, (self.getDataPrint()+"there is no <order>...</order>")

        db = self.tree.find("db").text
        fileDest = self.tree.find("fileDest").text
        orders = []

        for order in self.tree.findall("order"):
            assert order.text != None, ("in the mod "+self.modDir+", there is an empty <order></order>.")
            orders.append(order.text)

        modFinal = dbCopyHeadSingle(self,fileDest,{"db":db,"orders":orders})
