from psmod.logger import logger

class mod: # TODO: unittest
    """a base class for mod"""
    def __init__(self, parent, tree):
        self.logger = logger()
        self.logger.log("DEBUG","creating a mod of the type '"+self.name+"'.")
        self.logger.increment()
        self.parent = parent
        self.tree = tree # the xml tree of the mod
        self.modifiedFile = [] # a list of all the modified file ( file:singlemod )
        self.parent.conf.addMod(self)
        self.modDir = self.parent.modDir
        self.compute()
        self.logger.decrement()
        self.logger.log("DEBUG","creation of the mod finished")

    def compute(self):
        pass

    def getDataPrint(self):
        return "in the mod " + self.modDir + ", of the type "+self.name+", "
