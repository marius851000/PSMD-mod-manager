from lxml import etree
from psmod.logger import logger
from psmod.xmlParser import parseXML

class configuration:
    destDir = "./render/"
    modsEnabled = []
    flags = []

    # sert aussi à stocké les donné des mods
    db = {}
    modData = {} # name : name, modPack : mod(Object)
    mods = [] # list of mods
    smods = {} # list of single mods by file, like file1 : [smod1,smod2]

    actualID = 0

    setting = {}

    def __init__(self,configFile=None):
        assert type(configFile) == str or configFile==None

        self.logger = logger()
        if configFile != None:
            self.configFile = configFile
            self.readSetting(configFile)

    def readSetting(self,fileToRead):
        assert type(fileToRead) == str

        # read the file
        tree = parseXML(fileToRead)

        # search for mods
        for thing in tree.findall("*"):
            if thing.tag == "mod":
                modName = thing.text
                assert modName != None
                self.modsEnabled.append({"name":modName})

            elif thing.tag == "flag": # a flag
                fullFlag = thing.text
                flagMode = "add"
                assert fullFlag != None
                if fullFlag[0] == "-":#delete the flag ( if present )
                    flagMode = "del"
                    flagText = fullFlag[1:len(fullFlag)]
                else:
                    flagText = fullFlag
                assert flagText != ""
                if flagMode == "add":
                    if not flagText in self.flags:
                        self.flags.append(flagText)
                elif flagMode == "del":
                    self.flags.remove(flagText)
                else:
                    raise

            # TODO: tous rendre relatif au config.xml
            elif thing.tag == "setting":
                assert thing.text != None
                splitedEqual = thing.text.split("=")
                assert len(splitedEqual) == 2
                position = splitedEqual[0]
                toSet = splitedEqual[1]

                splitedLevel = position.split("/")

                previous = self.setting
                for level in splitedLevel[0:-1]:
                    if not level in previous:previous[level] = {}
                    previous = previous[level]

                if toSet.lower() == "true":
                    toSet = True
                elif toSet.lower() == "false":
                    toSet = False

                previous[splitedLevel[-1]] = toSet

    def toMake(self): # TODO: unittest
        def getLastType(thing): # TODO: unittest
            # str(type(thing)) generate something like ( for class ) <class 'a.b.c.d'>. we want d
            return str(type(thing)).split("'")[1].split(".")[-1]

        def getPriority(typee): # TODO: unittest
            # TODO: better to replace with something like "before a and after b" in the smod class
            prio = ["dbCopyHeadSingle","replaceFileSingle"]# + grand = + important
            prioD = {}
            nb = 0
            for thing in prio:
                nb += 1
                prioD[thing] = nb


            if typee in prioD: return prioD[typee]
            else:
                self.logger.log("WARNING","There is a single mod with no priority defined : " + typee)
                return -1

        def shortpriority(liste): # TODO: testunit
            listeValue = [] # toute la variété des valeur de priorité de liste
            for element in liste:
                priority = element["priority"]
                if not priority in listeValue: listeValue.append(priority)
            listeValue.sort()
            listeValue.reverse()

            listeShorted = []
            for priority in listeValue:
                for thing in liste:
                    if thing["priority"] == priority:
                        listeShorted.append(thing)

            return listeShorted

        def shortSmodPriority(smodList):
            smodDict = []
            for smod in smodList:
                smodDict.append({"priority":getPriority(getLastType(smod)),"smod":smod})
            smodShorted = shortpriority(smodDict)

            smodListNormal = []
            for smod in smodShorted:
                smodListNormal.append(smod["smod"])

            return smodListNormal

        self.logger.log("DEBUG","begin shorting single mods by file...","i")
        modsByFile = {}
        for smodF in self.smods:
            self.logger.log("ALL","shorting for the file : "+smodF)
            smods = self.smods[smodF]
            modsByFile[smodF] = shortSmodPriority(smods)
        self.logger.log("DEBUG","finished shorting single mods by file.","d")

        self.logger.log("INFO","generating make by file...","i")
        modsMake = ""
        for smodF in modsByFile:
            self.logger.log("DEBUG","generating make for the file : "+smodF,"i")
            smods = modsByFile[smodF]
            destFoldert = self.destDir+smodF
            destFolder = ""
            for space in destFoldert.split("/")[0:-1]:
                destFolder += space + "/"

            fileMake = "\tmkdir -p "+destFolder+"\n"
            dependance = []
            for smod in smods:
                self.logger.log("ALL","generating make for the smod : "+str(smod),"i")

                for dep in smod.dependance:
                    dependance.append(smod.modDir+dep)

                smodMakeTemp = smod.toMake()
                smodMake = ""
                for smodMakeLine in smodMakeTemp.split("\n"):
                    smodMake += "\n\t"+smodMakeLine
                smodMake += "\n\n"
                self.logger.log("ALL","generated.","d")
                fileMake += smodMake

            fileMakeFinal = self.destDir + smodF + " : "
            for dep in dependance:
                fileMakeFinal += dep + " "
            fileMakeFinal += "\n"
            fileMakeFinal += fileMake
            modsMake += fileMakeFinal
            self.logger.log("DEBUG","generated.","d")
        self.logger.log("INFO","generated make by file.","d")

        allMake = "all : " # cible all - le truc par défaut
        for smodF in modsByFile:
            allMake += self.destDir+smodF+" "

        makefile = allMake + "\n\n" + modsMake + "clean :\n\trm -r " + self.destDir
        return makefile

    def dict(self):
        mods = []
        for mod in self.modsEnabled:
            mods.append(mod["name"])
        dico = {"flags":self.flags,
            "modsDat":self.modsEnabled,
            "mods":mods,
            "setting":self.setting}

        return dico

    def addToDB(self,db,value):
        if not db in self.db : self.db[db] = []
        self.db[db].append(value)

    def getID(self):# TODO: unittest
        self.actualID += 1
        return self.actualID - 1

    def addSingleMod(self,smod): # TODO: unittest
        if not smod.file in self.smods: self.smods[smod.file] = []
        self.smods[smod.file].append(smod)

    def addMod(self,mod): # TODO: unittest
        self.mods.append(mod)

    def getConfig(self,location):
        previous = self.setting
        for thing in location.split("/"):
            previous = previous[thing]

        return previous

    @property
    def destDir(self):
        return self.getConfig("destination_directory")

    @property
    def modDir(self):
        return self.getConfig("mod_directory")
