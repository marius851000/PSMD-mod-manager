from lxml import etree
from psmod.logger import logger

def parseXML(fileURL,value={}):
    log = logger()
    log.log("DEBUG","file "+fileURL+" loading...")
    log.increment()
    fileTemp = open(fileURL)
    temp = fileTemp.read()
    fileTemp.close()
    tree = etree.fromstring(temp)

    for xif in tree.iter("if"):
        condition = xif.attrib.get("condition")
        render = eval(condition)
        log.log("ALL",condition+" returned "+str(render)+".")
        childParent = xif.getparent()
        elsee = xif.find("else")
        if render:

            if elsee != None:
                xif.remove(elsee)

            directChild = xif.getchildren()
            childParent.remove(xif)
            for child in directChild:
                childParent.append(child)
        else:
            if elsee != None:
                for child in elsee.getchildren():
                    childParent.append(child)
            childParent.remove(xif)

    for xinclude in tree.iter("include"):
        assert xinclude.text != None
        toInclude = parseXML(xinclude.text)
        xparent = xinclude.getparent()
        for xincludeElement in toInclude.findall("*"):
            xinclude.addnext(xincludeElement)
        xparent.remove(xinclude)

    for xraise in tree.iter("raise"):
        message = xraise.text # do unittest for this
        log.log("CRITICAL","raised due to an unfilled condition in the "+fileURL+" file : "+message)
        raise

    log.decrement()
    log.log("DEBUG","file "+fileURL+" loaded")
    return tree


def xmlToDict(tree): # TODO: write test
    etree.tostring(tree)
    render = {}
    for element in tree:
        render[element.tag] = element.text
    return render
