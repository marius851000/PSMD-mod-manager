import unittest

from testpsmod.test_logger import testLogger
from testpsmod.test_main import testPsmod
from testpsmod.test_configuration import testConfiguration
from testpsmod.test_mod_reader import testmodReader
from testpsmod.test_xml_reader import testxmlReader
#from testpsmod.test_mod import testMod # make unittest for mods and smods

if __name__ == '__main__':
    unittest.main()
