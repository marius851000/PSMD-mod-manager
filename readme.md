This is an libre ( free/open-source ) mod manager for pokemon super mystery dungeon and pokémon mystery dungeon : gate to the infinite ( while this isn't tested ), which allow to use multiple compatible mod at once.

It work ( tested ) on gnu/linux ( gentoo and arch ), but can also probably work with macOS and windows ( but with gnu emulator for windows, like cygwin or windows subsystem call for linux ). To make it run, you need make and python3.

To make it run, configure the file config.xml in the mods folder.
with the default setting, you MUST run the run.py script IN THE LMOD FOLDER.

how to use it :
- configure the config.xml in the mods folder
- go in the LMOD folder
- execute run.py with python 3 ( python3 run.py )
- clean the data from the previous compilation ( make clean)
- build the mod ( make -j thread), where thread is replaced by the number of thread you want to run at once ( if you dont know what it is put 5 )
- if this fail recommence from the clean step, but with only one thread to the last step. if it refail, contact me.

## mods included :
- menu_base : a menu which allow to select different mod which can be lauched at any moment ( like the freecam mod )
- menu_loader_other : allow to display the menu ( menu_base ) by pressing L and the other setting ( in the pause menu )
- camera-eddy : the original freecammod by eddyK28. only work on PSMD.
- camera2-eddy : the enhanced freecammod by eddyK28. Also allow to move the camera in dungeon. Originaly made for GTI, but work without problem on PSMD, however there is some thing you cant do with this version that you can on the original version ( camera-eddy ), like change your pokemon species.
