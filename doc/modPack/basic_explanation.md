# what is a modPack
A [modPack](modPack/modPack.md) is the name given to modifications that the end user can apply to the game. It is represented by a folder, which contain at least a [config.xml](modPack/config.xml.md) file, and optionnaly file to patch.
