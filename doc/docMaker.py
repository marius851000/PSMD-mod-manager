
_tabs = "\t"
class element:
    def __init__(self,data={},parent=None):
        self.parent = parent
        self.data = data
        self.child = []

    def addChild(self,data):
        self.child.append(element(data,self))
        return self.child[-1]

    def display(self,tab=0):
        tabc = tab * "\t"
        print(tabc+"data : "+str(self.data))
        print(tabc+"child :")
        for child in self.child:
            child.display(tab+1)

    def toHTML(self,baseURL="./",titleLevelBase=0,tablevel=0):
        tabc = "\n"+_tabs*tablevel
        retour = ""
        typee = self.data["type"]
        if typee == "base":
            retour += tabc+"<div class='base'>"
            for child in self.child:
                retour += child.toHTML(tablevel=tablevel+1)
            retour += tabc+"</div>"
        if typee == "line":
            if self.data["title"]:
                titleLevel = str(self.data["titleLevel"]+titleLevelBase)
                retour += tabc+"<h"+titleLevel+">"
                for child in self.child:
                    retour += child.toHTML(tablevel=tablevel+1)
                retour += tabc+"</h"+titleLevel+">"
            else:
                retour += tabc+"<p>"
                for child in self.child:
                    retour += child.toHTML(tablevel=tablevel+1)
                retour += tabc+"</p>"
        if typee == "paragraph":
            retour += tabc+self.data["text"]
        if typee == "link":
            retour += tabc+"<a href='"+baseURL+self.data["link"]+"'>"+self.data["text"]+"</a>"



        return retour

class elementHTML:
    def __init__(self,url):
        tempFile = open(url)
        self.html = tempFile.read()
        tempFile.close()
    def toHTML(self,baseURL=None,titleLevelBase=None,tablevel = 0):
        return self.html

class elementTXT:
    def __init__(self,url):
        tempFile = open(url)
        self.txt = tempFile.read()
        tempFile.close()
        txt2 = ""
        for loop in self.txt.split("\n"):
            txt2 += loop + "<br />\n"
        self.txt = txt2
    def toHTML(self,baseURL=None,titleLevelBase=None,tablevel = 0):
        return "<p>"+self.txt+"</p>"

def readMD(filee):
    def endOfActualChar(actu,aText):
        actu.data["text"]=aText
        actual = actu.parent
        return actual

    def startActualChar(typee,actu):
        actual = actu.addChild({"type":typee})
        return actual

    fileTemp = open(filee)
    md = fileTemp.read()
    fileTemp.close()

    render = element({"type":"base"})

    newline = True
    parsingTitle = True
    TitleLevel = 0
    actualLine = None
    actual = None
    actualText = ""
    inLink = False
    linkText = ""

    for char in md:
        if newline:
            newline = False
            if actual != actualLine:
                raise

            parsingTitle = False
            TitleLevel = 0
            actualText = ""
            actualLine = render.addChild({"type":"line","title":False})
            actual = actualLine
            if char == "#":
                parsingTitle = True
            else:
                actual = startActualChar("paragraph",actual)

        if char == "#" and parsingTitle:
            TitleLevel += 1
        elif parsingTitle:
            parsingTitle = False
            actualLine.data["title"]=True
            actualLine.data["titleLevel"]=TitleLevel
            actual = startActualChar("paragraph",actual)

        if not parsingTitle:
            if char == "[":
                inLink = 1
                actual = endOfActualChar(actual,actualText)
                actual = startActualChar("link",actual)
                actualText = ""
            elif char == "]" and inLink == 1:
                inLink = 2
                linkText = actualText
                actualText = ""
            elif char == "(" and inLink == 2:
                inLink = 3
            elif char == ")" and inLink == 3:
                actual.data["link"] = actualText
                actual = endOfActualChar(actual,linkText)
                actual = startActualChar("paragraph",actual)
                actualText = ""
            else:
                # normal character
                actualText += char

        if char == "\n":
            actualText = actualText[:-1]
            actual = endOfActualChar(actual,actualText)
            newline = True

    return render


def recursiveHtml(parsed,baseURL="./",level=1,tablevel=0):
    tabc = _tabs*tablevel
    retour = ""
    for loope in parsed:
        loop = parsed[loope]
        #retour += "<h"+str(level)+">"+loope+"</h"+str(level)+">"
        retour += tabc+"<p>"+loope+"</p>\n"
        retour += tabc+"<div class='box'>\n"
        if type(loop) != dict:
            retour += loop.toHTML(baseURL=baseURL,tablevel=tablevel)
        else:
            retour += recursiveHtml(loop,baseURL=baseURL,level=level,tablevel=tablevel+1)
        retour += tabc+"</div>\n"
    return retour


tempFile = open("fileToCompile.txt")
temp = tempFile.read()
tempFile.close()


parsed = {}
for text in temp.split("\n"):
    if text != "":
        UrlSplit = text.split("/")
        addIn = parsed
        for loop in UrlSplit[:-1]:
            if not loop in addIn:addIn[loop] = {}
            addIn = addIn[loop]
        if text[-3:] == ".md":
            addIn[UrlSplit[-1]] = readMD(text)
        elif text == "./fileToCompile.txt":
            pass
        elif text[-5:] == ".html":
            addIn[UrlSplit[-1]] = elementHTML(text)
        elif text[-4:] == ".txt":
            addIn[UrlSplit[-1]] = elementTXT(text)


header = """
<head>
    <style>
    .box {
        border : white;
        border-style : solid;
        padding : 1%;
        margin : 1%;
        margin-left: 0px;
        marge-right: 0px;
        background-color:grey
    }
    .box:hover {
        background-color:black;
    }
    </style>
</head>"""
print(header+recursiveHtml(parsed))
