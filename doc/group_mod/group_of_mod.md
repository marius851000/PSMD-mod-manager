# group of mod
## definition
A [group of mod](group_mod/group_of_mod.md) is a rassemblement of [mod pack](mod/mod_pack.md). That the thing that you compile with psmod. It is represented by a folder.
## files in the folder :
- [config.xml](group_mod/config.xml.md) is the file which indicate what mod to include, and various other parameter.
- one folder by [mod pack](mod/mod_pack.md)
