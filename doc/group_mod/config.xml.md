# config.xml
## what is config.xml
config.xml is a file which is used to configure a group of modification. It is used to determine which [modPack](mod/mod_pack.md) to include, [flag](group_mod/flag.md) to use, and various parameter ( for the full list, see [setting.xml](setting.xml)).
## config.xml format
the config.xml file is an [xml](doc/xml.html) file. The root tag should be <config>. So, the minimal configuration file is :
  <config>
  </config>
### the mod tag
The <mod> tag is used to enable a mod. the text should be the name of the folder containing the [modPack](mod/mod_pack.md) ( just the name ), and the [modPack](mod/mod_pack.md) folder should be in the root mods folder ( the same folder of the config.xml file )
### the flag tag
The <flag> tag is used to enable a ( global ) specific [flag](group_mod/flag.md). For example, to enable the DEBUG flag, you should write :
  <flag>DEBUG</flag>
flag which begin by a - is used to disable a flag. The following example disable the DEBUG flag :
  <flag>-DEBUG</flag>
It is hightly recommanded to put a flag for the game you want to use with this :
- <flag>PSMD</flag> if you use super mystery dungeon
- <flag>GTI</flag> if you use gate to infinite
for more information, see the [game page](game.md).
### the setting tag
The <setting> tag is used to modify / enable a global option. For example, to modify the render directory, use
  <setting>destination_directory=./renderDir</setting>
For the full list of setting, see the [setting page](setting.html)
