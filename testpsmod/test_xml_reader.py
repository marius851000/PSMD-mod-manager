import unittest
from psmod.logger import logger
logger.printlog = False
from psmod.xmlParser import parseXML

_xmlDir = "./testpsmod/xml/"
class testxmlReader(unittest.TestCase):
    def test_parseXML(self):
        xml = parseXML(_xmlDir+"dummy.xml")
        self.assertTrue(len(xml.findall("*")) == 1)

    def test_if(self):
        xml = parseXML(_xmlDir+"ifTrue.xml")
        self.assertEqual(xml.getchildren()[0].tag,"ok")

        xml = parseXML(_xmlDir+"ifFalse.xml")
        self.assertEqual(xml.getchildren(),[])

        xml = parseXML(_xmlDir+"ifMath.xml")
        self.assertEqual(xml.getchildren()[0].tag,"ok")
        self.assertEqual(xml.getchildren()[1].tag,"ok2")
        self.assertEqual(len(xml.getchildren()),2)

    def test_value(self):
        xml = parseXML(_xmlDir+"value.xml",{"flags":["GTI","DEBUG"],"mods":["mod2","mod1"]})

        self.assertEqual(xml.getchildren()[0].tag,"ok")
        self.assertEqual(xml.getchildren()[1].tag,"ok2")
        self.assertEqual(len(xml.getchildren()),2)







if __name__ == '__main__':
    unittest.main()
