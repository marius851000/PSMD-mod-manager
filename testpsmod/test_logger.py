import unittest
from psmod.logger import logger
logger.printlog = False

class testLogger(unittest.TestCase):
    def test_logger_load(self):
        log = logger()

    def test_logger_add_assertion(self):
        log = logger()

        # test assertion
        self.assertRaises(AssertionError, log.add_level, None)
        self.assertRaises(AssertionError, log.add_level, 1)

    def test_logger_add_single(self):
        """test adding a level ( which isnt dependant of another )"""
        log = logger()
        log.level = []

        log.add_level("THISISALEVEL")
        self.assertEqual(log.level,["THISISALEVEL"])

        log.add_level("THISISANOTHERLEVEL")
        self.assertEqual(log.level,["THISISALEVEL","THISISANOTHERLEVEL"])

    def test_logger_add_dependant(self):
        """adding a level which auto-add another level"""
        log = logger()
        log.level = []

        log.add_level("ERROR")
        self.assertEqual(log.level,["ERROR","CRITICAL"])

        log.add_level("IMPORTANT")
        self.assertEqual(log.level,["ERROR","CRITICAL","IMPORTANT"])

        log.add_level("THISISALEVEL")
        self.assertEqual(log.level,["ERROR","CRITICAL","IMPORTANT","THISISALEVEL"])

    def test_logger_add_no_overwrite(self):
        """adding 2 times the same level"""
        log = logger()
        log.level = []

        log.add_level("ALEVEL")
        log.add_level("ALEVEL")
        self.assertEqual(log.level,["ALEVEL"])

    def test_logger_log(self):
        log = logger()
        log.level = []
        log.history = []

        log.add_level("ALEVEL")
        log.log("ALEVEL","logged event")

        self.assertEqual(log.history[0]["level"],"ALEVEL")
        self.assertEqual(log.history[0]["message"],"logged event")


    def test_logger_log_assertion(self):
        log = logger()

        self.assertRaises(AssertionError, log.log, 1, "error")
        self.assertRaises(AssertionError, log.log, "error", 1)
        self.assertRaises(AssertionError, log.log, 1, 1)


if __name__ == '__main__':
    unittest.main()
