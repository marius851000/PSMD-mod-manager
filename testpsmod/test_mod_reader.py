import unittest
from psmod.logger import logger
logger.printlog = False
from psmod.modReader.modPack import modPack
from psmod.configuration.main import configuration

_modDir = "./testpsmod/mod/"
_confDir = "./testpsmod/configuration/"

class testmodReader(unittest.TestCase):
    confDummy = configuration(_confDir+"dummy.xml")

    def test_modPack_init(self):
        mp = modPack(_modDir+"dummy", self.confDummy)
        self.assertEqual(mp.modDir,_modDir+"dummy"+"/")

    def test_modPack_init_assertion(self):
        self.assertRaises(AssertionError, modPack, 1, self.confDummy)
        self.assertRaises(AssertionError, modPack, _modDir+"dummy", 1)

    def test_modPack_read_example(self):
        #just big test, TODO : smaller test
        conf = configuration(_confDir+"someflag.xml")
        mp = modPack(_modDir+"example", conf)
        self.assertEqual(conf.smods["romfs/script/mods/camera.lua"][0].toMake(),"cp ./testpsmod/mod/example/cam_PSMD.lua ./render/romfs/script/mods/camera.lua")



if __name__ == '__main__':
    unittest.main()
