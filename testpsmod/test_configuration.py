import unittest
from psmod.configuration.main import configuration

_confPath = "./testpsmod/configuration/"
class testConfiguration(unittest.TestCase):
    def test_configuration_load(self):
        conf = configuration(_confPath+"dummy.xml")

    def test_configuration_init_assertion(self):
        self.assertRaises(AssertionError,configuration,1)

    def test_configuration_init(self):
        conf = configuration(_confPath+"dummy.xml")
        self.assertEqual(conf.configFile,_confPath+"dummy.xml")
        from psmod.logger import logger
        self.assertEqual(type(conf.logger),logger)

    def test_configuration_readSetting_assertion(self):
        conf = configuration(_confPath+"dummy.xml")
        self.assertRaises(AssertionError,conf.readSetting,1)

        self.assertRaises(AssertionError,configuration,_confPath+"modempty.xml")
        self.assertRaises(AssertionError,configuration,_confPath+"flagempty.xml")
        self.assertRaises(AssertionError,configuration,_confPath+"flagdelempty.xml")


    def test_configuration_readSetting(self):
        conf = configuration(_confPath+"dummy.xml")

        conf = configuration(_confPath+"somemod.xml")
        self.assertTrue(conf.modsEnabled[0]["name"] == "mod1" and conf.modsEnabled[1]["name"] == "mod2")

        conf = configuration(_confPath+"someflag.xml")
        self.assertEqual(conf.flags,["PSMD","DEBUG"])

        conf = configuration(_confPath+"all.xml")
        self.assertEqual(conf.flags,["PSMD","DEBUG"])
        self.assertTrue(conf.modsEnabled[0]["name"] == "mod1" and conf.modsEnabled[1]["name"] == "mod2")

    def test_configuration_dict(self):
        conf = configuration(_confPath+"all.xml")
        self.assertEqual(conf.dict()["mods"],["mod1","mod2"])
        self.assertEqual(conf.dict()["flags"],["PSMD","DEBUG"])








#flagempty
#flagdelempty

if __name__ == '__main__':
    unittest.main()
