import unittest
from psmod.main import psmod

baseMod = "./mods/config.xml"

class testPsmod(unittest.TestCase):
    def test_psmod_load(self):
        psm = psmod(baseMod)

    def test_psmod_init_assertion(self):
        self.assertRaises(AssertionError,psmod,baseMod,log="UNEXISTANTLOGLEVEL")
        a = psmod(baseMod,log="CRITICAL")
        self.assertRaises(AssertionError,psmod,1)

    def test_psmod_init(self):
        # test if the logger is loaded
        psm = psmod(baseMod)
        from psmod.logger import logger
        self.assertTrue(type(psm.logger) == logger)






if __name__ == '__main__':
    unittest.main()
